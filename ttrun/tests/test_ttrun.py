# Copyright (c) 2016 Red Hat, Inc
#
# This file is part of ttrun
#
# ttrun is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# ttrun is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Ansible.  If not, see <http://www.gnu.org/licenses/>.

"""
test_ttrun
----------------------------------

Tests for `ttrun` module.
"""

import testtools


class TestTtrun(testtools.TestCase):

    def test_something(self):
        pass
