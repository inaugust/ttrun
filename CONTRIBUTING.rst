=====================
Contributing to ttrun
=====================

ttrun probably doesn't want to grow a ton of features. However, if you want
to contribute, please email patches to mordred@inaugust.com
